#!/usr/bin/env python3.11

import os
import shutil
import tempfile
import sys
import argparse
from argparse import RawTextHelpFormatter
from PyQt5.QtCore import QUrl, Qt
from PyQt5.QtGui import QKeySequence
from PyQt5.QtWidgets import QApplication, QMainWindow, QAction, QToolBar, QLineEdit, QPushButton, QShortcut, QTextEdit, QPlainTextEdit, QVBoxLayout, QWidget
from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEngineSettings
from PyQt5.QtNetwork import QNetworkProxy
from urllib.parse import urlencode


class SimpleBrowser(QMainWindow):
    def __init__(self):
        super().__init__()
        self.init_ui()


    def init_ui(self):
        self.setWindowTitle("Web Browser")
        self.setGeometry(100, 100, 800, 600)

        self.shortcut_close = QShortcut(QKeySequence(Qt.CTRL + Qt.Key_I), self)
        self.shortcut_close.activated.connect(self.view_page_source)

        self.shortcut_close = QShortcut(QKeySequence(Qt.CTRL + Qt.Key_D), self)
        self.shortcut_close.activated.connect(self.close)

        self.webview = QWebEngineView()
        self.webview.settings().setAttribute(QWebEngineSettings.JavascriptEnabled, False)
        self.webview.settings().setAttribute(QWebEngineSettings.JavascriptCanOpenWindows, False)
        self.webview.settings().setAttribute(QWebEngineSettings.JavascriptCanAccessClipboard, False)
        self.webview.settings().setAttribute(QWebEngineSettings.LocalStorageEnabled, False)
        self.webview.settings().setAttribute(QWebEngineSettings.LocalContentCanAccessRemoteUrls, False)
        self.webview.settings().setAttribute(QWebEngineSettings.WebGLEnabled, False)
        self.webview.settings().setAttribute(QWebEngineSettings.PluginsEnabled, False)
        self.webview.settings().setAttribute(QWebEngineSettings.ScreenCaptureEnabled, False)
        self.webview.settings().setAttribute(QWebEngineSettings.LocalStorageEnabled, False)
        self.webview.settings().setAttribute(QWebEngineSettings.LocalContentCanAccessRemoteUrls, False)
        self.webview.settings().setAttribute(QWebEngineSettings.DnsPrefetchEnabled, False)
#        self.webview.settings().setAttribute(QWebEngineSettings.AutoLoadImages, False)

        self.temp_dir = tempfile.TemporaryDirectory()
        storage_path = os.path.join(self.temp_dir.name, "QtWebEngine")
        self.webengine_data_path = os.path.join(storage_path, "Default")
        self.webview.page().profile().setPersistentStoragePath(self.webengine_data_path)

        user_agent = "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36"
        self.webview.page().profile().setHttpUserAgent(user_agent)

        self.address_bar = QLineEdit()
        self.address_bar.returnPressed.connect(self.load_page)

        self.go_button = QPushButton("Search")
        self.go_button.clicked.connect(self.load_page)

        toolbar = QToolBar()
        toolbar.addWidget(self.address_bar)
        toolbar.addWidget(self.go_button)

        reload_action = QAction("Reload", self)
        reload_action.triggered.connect(self.webview.reload)
        toolbar.addAction(reload_action)

        back_action = QAction(" < ", self)
        back_action.triggered.connect(self.webview.back)
        toolbar.addAction(back_action)

        forward_action = QAction(" > ", self)
        forward_action.triggered.connect(self.webview.forward)
        toolbar.addAction(forward_action)

        close_action = QAction("Quit", self)
        close_action.triggered.connect(self.close_browser)
        toolbar.addAction(close_action)

        view_source_action = QAction("Page Source", self)
        view_source_action.triggered.connect(self.view_page_source)
        toolbar.addAction(view_source_action)

        self.addToolBar(toolbar)

        self.setCentralWidget(self.webview)

        if args.white:
            self.setStyleSheet("background-color: #ffffff; color: #000000;")
        else:
            self.setStyleSheet("background-color: #121212; color: yellow;")


        self.webview.urlChanged.connect(self.update_address)


    def view_page_source(self):
        self.webview.page().toHtml(self.page_source_callback)


    def page_source_callback(self, html):
        source_window = QMainWindow(self)
        source_window.setWindowTitle("View Page Source")
        source_window.setGeometry(100, 100, 800, 600)

        text_edit = QTextEdit(source_window)
        text_edit.setPlainText(html)
        text_edit.setReadOnly(True)

        source_window.setCentralWidget(text_edit)

        exit_button = QPushButton("Exit", source_window)
        exit_button.clicked.connect(source_window.close)

        layout = QVBoxLayout()
        layout.addWidget(text_edit)
        layout.addWidget(exit_button)

        central_widget = QWidget()
        central_widget.setLayout(layout)

        source_window.setCentralWidget(central_widget)
        source_window.show()


    def update_address(self, url):
        self.address_bar.setText(url.toString())


    def load_page(self):
        url = self.address_bar.text()

        if args.url:
            url = args.url

        if not url.startswith('http://') and not url.startswith('https://'):
            search_query = urlencode({'q': url})
            url = f'https://html.duckduckgo.com/html/?{search_query}'
        self.webview.setUrl(QUrl(url))


    def close_browser(self):
        self.temp_dir.cleanup()
        if os.path.exists(self.webengine_data_path):
            shutil.rmtree(self.webengine_data_path)
        self.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Web Browser with optional dark mode.",
                                     formatter_class=RawTextHelpFormatter,
                                     epilog="Examples:\n"
                                         "./simple_web_browser.py\n"
                                         "./simple_web_browser.py --white\n"
                                         "./simple_web_browser.py --white --proxy socks5://127.0.0.1:9050")
    parser.add_argument("--white", action="store_true", help="Enable white mode.")
    parser.add_argument("--proxy", help="SOCKS proxy address (e.g., 'socks5://127.0.0.1:9050').")
    parser.add_argument("--url", help="URL to open in the browser.")
    args = parser.parse_args()

    if args.proxy:
        proxy_type, proxy_addr = args.proxy.split("://")
        proxy = QNetworkProxy(QNetworkProxy.Socks5Proxy if proxy_type.lower() == "socks5" else QNetworkProxy.Socks5Proxy)
        proxy.setHostName(proxy_addr.split(":")[0])
        proxy.setPort(int(proxy_addr.split(":")[1]))
        QNetworkProxy.setApplicationProxy(proxy)

    app = QApplication(sys.argv)
    browser = SimpleBrowser()
    browser.show()
    sys.exit(app.exec_())
